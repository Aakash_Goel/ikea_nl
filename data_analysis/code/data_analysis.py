# import libraries
import pandas as pd
import numpy as np
from polyglot.detect import Detector
from polyglot.detect.base import UnknownLanguage
from bs4 import BeautifulSoup
import sys
stdout = sys.stdout
reload(sys)
sys.setdefaultencoding('utf-8')
sys.stdout = stdout
pd.set_option('display.height', 1000)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.max_colwidth', -1)

#Reading Data
data = pd.read_excel('../data/RFC Text and tags.xlsx')

print(data.shape)
data.dtypes

# #### Check if any column have missing value

pd.isnull(data).sum()

# #### check - how many rows are unique

data.drop_duplicates(subset='TEKST',keep='first').shape

# #### check duplicate rows

data[data.duplicated(subset='TEKST',keep='first')]['TEKST']

# #### Duplicate data presence across channels

data[data.duplicated(subset='TEKST',keep='first')]['ID kanaal'].value_counts()

# #### check some special cases of text

data[data['TEKST']=='(Email contained no body)\n'].head(3)

# #### Channel counts

data['ID kanaal'].value_counts()

# #### channel and source of communication count

data.groupby(['ID kanaal','Type item thread']).size()

# data[(data['ID kanaal']=='E-mail') & (data['Type item thread']=='Klant-proxy')]

# columns containing 'Nee' or 'Ja'
col = ['About IKEA','Assembly','Assembly Service','Assistance','Buying Assistance','Campaigns',       'Cancel Order','Change Delivery','Change/Cancel','Check Status','checkRFC','Click & Collect',       'Complaint','Co-Worker','Customer','Customer Support','Damaged','Delayed',       'Delivery Service','Giftcard','Give Feedback','Home Delivery','hulp bij aanschaf','IKEA Business',       'IKEA Catalogue','IKEA Family','Incomplete Info','Info About order','Information','Installation',       'Kitchen','Lost & Found','Missing','Missing Info','Mobile App','Online Shop','Payment Options',       'Pick up Point','Picking Service','Product Recall','Quality','Recycling Service','Report',       'Return/Exchange','rfc_changeorder','rfc_check','rfc_homefurnish','rfc_ia_a_openaccount',       'rfc_ia_o_changeorder','rfc_ia_s_sac','rfc_internal','rfc_localactivity','rfc_measuring',       'rfc_product','rfc_serviceprovider','rfc_socialmedia','Safety Issue','Service Provider',       'Sewing Service','Shop in Store','Spare Parts','Stock Check','Store','Store Info','Tech Support',       'Warranties','Website'
]

x=data[col]

x['About IKEA'].unique()

print(x.shape)

# #### check - which columns have only 'Naa' or 'Ja' as value

no_list  = []
yes_list = []
no_yes_list = []

for i in col:
    if len(x[i].unique())==2:
        no_yes_list.append(i)
    elif len(x[i].unique())==1:
        if 'Nee' in x[i].unique()[0]:
            no_list.append(i)
        if 'Ja' in x[i].unique()[0]:
            yes_list.append(i)
    print(i)
    print(x[i].unique())
    print("++++++++++++++++++++")

print(yes_list)
print(no_list)
print(no_yes_list)

# #### Distribution of labels 

for i in no_yes_list:
    print(i)
    print(x[i].value_counts(normalize=True))
    print("+++++++++++++++++")

print(x.describe())

x.replace('Nee',0,inplace=True)
x.replace('Ja',1,inplace=True)

print(x.head())

print(x.sum(axis=0))

# #### check - language distribution

def language_detection(text):
	text = str(text)
	print(text)
	print("++")
	try:
		detector = Detector(text.encode('utf-8'))
		lang_code = detector.language.code
		return lang_code
	except UnknownLanguage:
		return 'UNK'

def clean_up_text(text):
	#Strip of tags from text using Beautiful Soup
	soup = BeautifulSoup(text, 'lxml')
	soup = soup.get_text()
	soup = soup.strip('\n').strip("\t").strip("\r").replace("\n"," ").replace("\t"," ").replace("\r"," ")
	# Remove URLS..
	# p.set_options(p.OPT.URL)
	# soup = p.clean(soup)
	# reg_http=r"(http)(s)?(://)?"
	# soup = re.sub(reg_http,"", soup)
	return soup

data['TEKST_wo_html'] = data['TEKST'].apply(clean_up_text)
data['lang_code'] = data['TEKST_wo_html'].apply(language_detection)
data['lang_code'].value_counts()